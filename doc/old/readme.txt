1-Instructions to install and configure prerequisites or dependencies
The application just need the Java SE 8 JDK and Maven to compile, execute the test and run.
Instructions to install maven could be found at http://maven.apache.org/

2. Instructions to create and initialize the database
The application uses an embedded version of the memory database H2.
The database schema is coded inside the application, using entity POJOs, then no actions regards the database is needed.

3. Assumptions you have made (a good quality response will include your thought process and assumptions)

The first assumption was to make a simple application regards the system requirements. Following the agile processes, an 
architecture too much elaborated could bring unnecessary complexity and not give functionality to the client.
The business model was focused on the main entity, the Ticket. The ticket contains a relation with the user, the flight
and the seat in that flight. The other entity I let simplified, in particular the Flight, where I just put the name field,
what include many information like connection, origins and destinations.

A more elaborate analysis could include a more detailed Flight information, including Airplane model, airline company,
routes, connections and seats disposition in the plane. But that was not included in the project.
I choose to use Rest Api to do the communications between the modules of the application, because it could 
help with the SPA implementation and provide an infrastructure to implement a future mobile phone connection. 
For that I used Spring Rest API.

I used Spring Framework because of the easy to use, and the many functionalities what can be plugged on it.
Spring Boot provides a good test environment, making the software almost independent of other software's dependencies.
Spring Social for Facebook and Spring Security give a solid authentication and authorization framework, with just a limited configuration.

To connect the Public User to the system I used OAuth2 API and for the Staff user, form authentication.
To do the SPA, I choose the AngularJS API and Bootsrap CSS. API that provides a huge amount of functionalities and option,
and a large community. AngularJS provide a Rest API what can be used to communication and autheticantion.

I choose to use scheduled durable JMS messages to process the messages and emails, then this will not be loose in a system failure.
The email sends the to an SMTP server using Java Mail API.


4. Requirements that you have not covered in your implementation
Unfortunately the current application code lacks the following requirements:
- Login with Staff user, using form authentication
- Departure time on the flight and this search option on the SPA.
- Ticket operation of Buying, checking and cancel
- JMS connection to handle the messages
- Email services to send emails to user and staff

5. Instructions to configure and prepare the source code to build and run properly
The application is fully configured, and the current functionality work without other setting.
To compile the code, go to the base directory, where the pom.xml is found, and execute:
mvn compile

To excute the Unit tests:
mvn test

And to start the application:
mvn spring-boot:run

The application can be accessed at the URL: http://localhost:8080

6. Issues you have faced while completing the assignment, if any
I had trouble making the communication between the AngularJS application with the Rest services and the Security Framework.
It took me a lot of my assignment time.


7. Any feedback you wish to give for improving the assignment and our process.
I think that assignment requires more that just 8 hours, and maybe three days is insufficient.
Expanding it to 5 or more days could give your company results with more quality.
