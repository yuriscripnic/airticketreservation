package airticket.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import airticket.model.Flight;
import airticket.model.Seat;
import airticket.model.Ticket;
import airticket.model.User;
import airticket.repository.FlightRepository;
import airticket.repository.TicketRepository;
import airticket.repository.UserRepository;
//import net.minidev.json.parser.JSONParser;

@RestController
public class Actions {
	
	private static final Logger log = LoggerFactory.getLogger(Actions.class);
	
	@Autowired
	private FlightRepository flightRepository;
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private UserRepository userRepository;
	

	@RequestMapping(value = "/ticket/{ticket_id}/cancel",method = RequestMethod.GET)
	public void cancelTicketPublic(@PathVariable("ticket_id") long ticket_id){
		log.debug("cancelTicket:"+ticket_id);
		System.out.println("cancelTicket:"+ticket_id);
		Ticket ticket = ticketRepository.findOne(ticket_id);
		ticketRepository.delete(ticket);
	}
	
	
	@RequestMapping(value = "/staff/flight/{flight_id}/cancel",method = RequestMethod.GET)
	public void cancelFlight(@PathVariable("flight_id") long flight_id){
		log.debug("cancelFlight:"+flight_id);
		System.out.println("cancelFlight:"+flight_id);
		Flight flight =  flightRepository.findOne(flight_id);
		List<Ticket> tickets = ticketRepository.findByFlight(flight);
		log.debug("cancelFlight.tickets:"+tickets);
		System.out.println("cancelFlight.tickets:"+tickets);
		for(Ticket t : tickets){
			ticketRepository.delete(t);
		}
		flightRepository.delete(flight);
	}
	
	
	@RequestMapping(value = "/ticket/{ticket_id}/checking",method = RequestMethod.GET)
	public void doChecking(@PathVariable("ticket_id") long ticket_id){
		Ticket ticket = ticketRepository.findOne(ticket_id);
		ticket.setCheckingDone(true);		
		ticketRepository.save(ticket);
	}
	
	@RequestMapping(value = "/ticket/buy",method = RequestMethod.POST)
	public void buyTicket(
			     @RequestParam("flight_id") long flight_id,
			     @RequestParam("user_id")   long user_id,
			     @RequestParam("seat")      int seat
			     ){
		Flight flight =  flightRepository.findOne(flight_id);
		Ticket ticket = ticketRepository.findByFlightAndSeat(flight, seat);
		if (ticket==null) {
			User user = userRepository.findOne(user_id);
			ticket = new Ticket(user, flight, seat);
		}
		ticketRepository.save(ticket);
	}
	
	@RequestMapping(value = "/ticket/{ticket_id}/print",method = RequestMethod.GET)
	public String printPass(@PathVariable("ticket_id") long ticket_id){
		Ticket ticket = ticketRepository.findOne(ticket_id);
		String pass = ticket.printPass();		
		return pass;
	}
	
    @RequestMapping(value = "/seat/{flight_id}", method = RequestMethod.GET)
    public List<Seat> listAllSeatsByFlights(@PathVariable("flight_id") long flight_id) {
    	Flight flight = flightRepository.findOne(flight_id);
    	List<Ticket> tickets = (List<Ticket>)ticketRepository.findByFlight(flight);
    	int numSeats = flight.getNumberOfSeats();
    	
    	Seat seats[] = new Seat[numSeats];
    	for(int i = 0; i < numSeats;i++){
    		seats[i] = new Seat(null,i+1,"empty","/ticket/buy/");
    	}
		ListIterator<Ticket> it = tickets.listIterator(); 
		while(it.hasNext()){
			Ticket ticket = it.next();
			int seatNumber =  ticket.getSeat();
			Seat seat = seats[seatNumber-1];
			seat.setTicket(ticket);
			if (ticket.isCheckingDone()){
				seat.setStatus( "checkingDone");
				seat.setAction("/ticket/"+ticket.getId()+"/cancel");
			}else {
				seat.setStatus( "reserved");
				seat.setAction("/ticket/"+ticket.getId()+"/checking");
			}
		}
    	return Arrays.asList(seats);
    }

}


/*
    	//List<Seat> seats = new ArrayList<Seat>(); 
for(int i = 1; i <= flight.getNumberOfSeats();i++){
Seat seat = new Seat(i,-1,false,false);
ListIterator<Ticket> it = tickets.listIterator(); 
while(it.hasNext()){
	Ticket t = it.next();     		
	if(t.getSeat()==i) {
		seat.setTicketId(t.getId());
		seat.setReserved(true);
		seat.setCheckingDone(t.isCheckingDone());
	}
}
seats.add(seat);
}*/
