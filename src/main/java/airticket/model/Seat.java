package airticket.model;

public class Seat{
	private Ticket ticket;
	private int number;
	private String status;
	private String action;
	
	public Seat(Ticket ticket, int number,String status, String action) {
		this.number = number;
		this.ticket = ticket;
		this.status = status;
		this.action = action;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
	
}
/*public class Seat {
	private int number;
	private boolean reserved;	
	private boolean checkingDone;
	private long ticketId;
	
	public Seat(int number, long ticket_id,boolean reserved, boolean checkingDone) {
		this.number = number;
		this.reserved = reserved;
		this.checkingDone = checkingDone;
		this.setTicketId(ticket_id);
		
	}
	public boolean isCheckingDone() {
		return checkingDone;
	}
	public void setCheckingDone(boolean checkingDone) {
		this.checkingDone = checkingDone;
	}
	public boolean isReserved() {
		return reserved;
	}
	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}

	public long getTicketId() {
		return ticketId;
	}
	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}
}*/
