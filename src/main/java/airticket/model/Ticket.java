package airticket.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Ticket{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	private int seat;
    private boolean checkingDone;
    
    @ManyToOne( targetEntity=User.class )
    private User user;
    
	@ManyToOne( targetEntity=Flight.class )
    private Flight flight;

    protected Ticket() {}

    public Ticket(User user, Flight flight , int seat) {
    	this.setUser(user);
    	this.setFlight(flight);
        this.setSeat(seat);
        this.setCheckingDone(false);
    }

	public int getSeat() {
		return seat;
	}

	public void setSeat(int seat) {
		this.seat = seat;
	}
	
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public boolean isCheckingDone() {
		return checkingDone;
	}

	public void setCheckingDone(boolean checkingDone) {
		this.checkingDone = checkingDone;
	}
	
	public void sendMessage(){
		// Send Email Message
	}
	
	public String printPass(){
		StringBuffer sb = new StringBuffer();
		sb.append("***** Reservation Ticket ******\n");
		sb.append("Name   : "+ user.getFirstName() + " "+ user.getLastName()+ "\n");
		sb.append("Flight : "+ flight.getName() + "\n");
		sb.append("Seat   : "+ seat + "\n");
		return sb.toString();
	}
}
