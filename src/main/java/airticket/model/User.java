package airticket.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class User{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String facebookId;
    private boolean staff=false;
    private String password=null;
    
	public User() {}

    public User(String firstName, String lastName,String email) {
    	this.staff =  false;
    	this.password = null;
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setEmail(email);
    }
    
    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public boolean getStaff() {
		return staff;
	}

	public void setStaff(boolean isStaff) {
		this.staff = isStaff;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "{ id:" + id + ", firstName:" + firstName + ", lastName:" + lastName + ", email:" + email + "}";
	}
	
	
}
