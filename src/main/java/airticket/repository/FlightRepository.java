package airticket.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import airticket.model.Flight;


@RepositoryRestResource(collectionResourceRel = "flight", path = "flight")
public interface FlightRepository extends PagingAndSortingRepository<Flight, Long> {
    Flight findByName(@Param("name")String name);
}
