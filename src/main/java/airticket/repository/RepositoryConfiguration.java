package airticket.repository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import airticket.model.Flight;
import airticket.model.Ticket;

@Configuration
public class RepositoryConfiguration extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    	//config.setBasePath("/api");
        config.exposeIdsFor(Ticket.class);
        config.exposeIdsFor(Flight.class);
    }
}