package airticket.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import airticket.model.User;

@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserRepository extends CrudRepository<User, Long> {
    User findByFirstName(@Param("name")String firstName);
    User findByFacebookId(@Param("facebookId") String facebookId);
    User findByEmail(@Param("email") String email);
    
    //@RestResource(exported = false)
    //<S extends User> S save(S entity);
}
