package airticket.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import airticket.model.Flight;
import airticket.model.Ticket;
import airticket.model.User;


@RepositoryRestResource(collectionResourceRel = "ticket", path = "ticket")
public interface TicketRepository extends CrudRepository<Ticket, Long> {
	List<Ticket> findByUser(@Param("user") User user);
	List<Ticket> findByFlight(@Param("flight") Flight flight);
	Ticket findByFlightAndSeat(@Param("flight") Flight flightName,@Param("seat") int seat);
}
