'use strict';

var app = angular.module('myApp', ['ngRoute','spring-data-rest'])
	.config(
		function($httpProvider) {
			$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		})
    .controller('FlightController', function ($scope, $http,$location, SpringDataRestAdapter) {
        var httpPromise = $http.get('/flight').success(function (response) {
            $scope.response = angular.toJson(response, true);
        });

        SpringDataRestAdapter.process(httpPromise).then(function (processedResponse) {
            $scope.flights = processedResponse._embeddedItems;
            $scope.processedResponse = angular.toJson(processedResponse, true);
        });
        
    	$scope.getAllSeats = function(flight_id){
    		console.log("getAllSeats.flight_id="+flight_id);
    	    var httpPromise = $http.get('/seat/'+flight_id).success(function (response) {
    	        $scope.seats = response;
    	    })
	    
    	};
    	$scope.goToLink = function(link){
    		$location.path(link);
    	}
    	
    	$scope.seatAction = function(action){
    		console.log("seatAction.action="+action);
    	};
	})
	.controller("home", function($http, $location) {
					var self = this;
					$http.get("/user").success(function(data) {
						if(data.userAuthentication !=null){
							self.user = data.userAuthentication.details.name;
							self.authenticated = true;
						}else{
							self.user = "N/A";
							self.authenticated = false;
						}
					}).error(function() {
						self.user = "N/A";
						self.authenticated = false;
					});
					
					self.login = function() {
							$location.path("/login/facebook");
					};
					
					self.logout = function() {
						$http.post('logout', {}).success(function() {
							self.authenticated = false;
							$location.path("/");
						}).error(function(data) {
							console.log("Logout failed")
							self.authenticated = false;
						});
					};
		})
		.controller('navigation',

function($rootScope, $http, $location, $route) {
	
	var self = this;

	self.tab = function(route) {
		return $route.current && route === $route.current.controller;
	};

	var authenticate = function(callback) {

		$http.get('user').then(function(response) {
			if (response.data.name) {
				$rootScope.authenticated = true;
			} else {
				$rootScope.authenticated = false;
			}
			callback && callback();
		}, function() {
			$rootScope.authenticated = false;
			callback && callback();
		});

	}

	authenticate();

	self.credentials = {};
	self.login = function() {
		$http.post('login', $.param(self.credentials), {
			headers : {
				"content-type" : "application/x-www-form-urlencoded"
			}
		}).then(function() {
			authenticate(function() {
				console.log("$rootScope.authenticated:"+$rootScope.authenticated)
				if ($rootScope.authenticated) {
					console.log("Login succeeded")
					$location.path("/");
					self.error = false;
					$rootScope.authenticated = true;
				} else {
					console.log("Login failed with redirect")
					$location.path("/login");
					self.error = true;
					$rootScope.authenticated = false;
				}
			});
		}, function() {
			console.log("Login failed")
			$location.path("/login");
			self.error = true;
			$rootScope.authenticated = false;
		})
	};

	self.logout = function() {
		$http.post('logout', {}).finally(function() {
			$rootScope.authenticated = false;
			$location.path("/");
		});
	}

});
		
