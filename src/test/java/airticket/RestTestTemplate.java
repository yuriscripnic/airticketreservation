package airticket;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import airticket.model.Flight;
import airticket.model.User;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SocialApplication.class)
@WebAppConfiguration
public class RestTestTemplate {
	@Autowired
	protected WebApplicationContext ctx;

	protected MockMvc mockMvc;
	
	protected User user;
	protected Flight flight;
	private Gson gson = new Gson();

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
		user = new User ("Yuri","Scripnic","yuriscripnic@hotmail.com"); 
		flight = new Flight("Flight 1",10);
	}
	
	protected String convertObjectToJson(Object o) throws IOException {
		return gson.toJson(o); 
    }
	
	protected void buyTicket(long flight_id,long user_id,int seat) throws IOException, Exception {
		this.mockMvc.perform(post("/ticket/buy")
				.param("flight_id", Long.toString(flight_id))
				.param("user_id", Long.toString(user_id))
				.param("seat", Integer.toString(seat)))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
	}

	protected void createUser(User user) throws IOException, Exception {
		byte[] msg;
		msg = convertObjectToJson(user).getBytes();
		this.mockMvc.perform(post("/user")
		  .contentType(MediaType.APPLICATION_JSON)
		  .content(msg))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
	}


	protected MvcResult createFlight(Flight flight) throws Exception {
		byte[] msg = convertObjectToJson(flight).getBytes();
		MvcResult result = this.mockMvc.perform(post("/flight")
		  .contentType(MediaType.APPLICATION_JSON)
		  .content(msg))
		.andDo(print())
		.andExpect(status().is2xxSuccessful())
		.andReturn();
		return result;
	}

}
