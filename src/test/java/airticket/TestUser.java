package airticket;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.io.IOException;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import airticket.model.User;


public class TestUser extends RestTestTemplate{
	
	@Test
	public void testCrudUser() throws Exception{
		byte[] msg = convertObjectToJson(new User("Yuri","Scripnic","yuriscripnic@hotmail.com")).getBytes();
		byte[] newmsg = convertObjectToJson(new User("Test","Test","test@test.com")).getBytes();
		
		//Create User
		this.mockMvc.perform(post("/user")
		  .contentType(MediaType.APPLICATION_JSON)
		  .content(msg))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
		
		// Read User Data
		this.mockMvc.perform(get("/user/1","json").accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(jsonPath("$.firstName").value("Yuri"))
				.andExpect(jsonPath("$.lastName").value("Scripnic"))
				.andExpect(jsonPath("$.email").value("yuriscripnic@hotmail.com"))
				.andExpect(status().isOk());
		
		// Update User Data
		this.mockMvc.perform(put("/user/1","json")
				  .contentType(MediaType.APPLICATION_JSON)
				  .content(newmsg)
    			  .accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(jsonPath("$.firstName").value("Test"))
				.andExpect(jsonPath("$.lastName").value("Test"))
				.andExpect(jsonPath("$.email").value("test@test.com"))
				.andExpect(status().is2xxSuccessful());
		
		// Delete User Data
		this.mockMvc.perform(delete("/user/1","json"))
				.andDo(print())
				.andExpect(status().is2xxSuccessful());
		
		// Verify if User is deleted
		this.mockMvc.perform(get("/user/1","json"))
		.andDo(print())
		.andExpect(status().is4xxClientError());		
	}
}
