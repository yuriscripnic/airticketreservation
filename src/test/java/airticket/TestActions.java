package airticket;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import airticket.model.Flight;
import airticket.model.Ticket;
import airticket.model.User;



public class TestActions extends RestTestTemplate{

	//@Test
	public void testCancelSeat() throws Exception{
		byte[] msg = convertObjectToJson(new Ticket(user,flight,1)).getBytes();		
		//Create Ticket
		this.mockMvc.perform(post("/ticket")
		  .contentType(MediaType.APPLICATION_JSON)
		  .content(msg))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
		
		// Cancel Ticket
		this.mockMvc.perform(get("/ticket/1/cancel","json"))
				.andDo(print())
				.andExpect(status().is2xxSuccessful());
		
	
		// Verify if Ticket is cancelled
		this.mockMvc.perform(get("/ticket/1","json"))
		.andDo(print())
		.andExpect(status().is4xxClientError());		
	}
	
	//@Test
	public void testCancelFlight() throws Exception{
		
		System.out.println("testCancelFlight:Create Flight");
		MvcResult result = createFlight(flight);
		
		String f =  result.getResponse().getContentAsString();
		System.out.println("testCancelFlight result:"+f);
		
		System.out.println("testCancelFlight:Create Ticket 1");
		byte[] msg = convertObjectToJson(new Ticket(user,flight,1)).getBytes();
		this.mockMvc.perform(post("/ticket")
		  .contentType(MediaType.APPLICATION_JSON)
		  .content(msg))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
		
		System.out.println("testCancelFlight:Create Ticket 2");
		msg = convertObjectToJson(new Ticket(user,flight,2)).getBytes();		
		this.mockMvc.perform(post("/ticket")
		  .contentType(MediaType.APPLICATION_JSON)
		  .content(msg))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
		
		System.out.println("testCancelFlight:Cancel Flight");
		this.mockMvc.perform(get("/staff/flight/1/cancel","json"))
				.andDo(print())
				.andExpect(status().is2xxSuccessful());
		
	
		System.out.println("testCancelFlight: Verify if Tickets are cancelled");
		this.mockMvc.perform(get("/ticket/1","json"))
		.andDo(print())
		//.andExpect(jsonPath("$..ticket").value(""));
		.andExpect(status().is2xxSuccessful());
		
		//this.mockMvc.perform(get("/ticket/3","json"))
		//.andDo(print())
		//.andExpect(status().is4xxClientError());
		
		// Verify if Flight is cancelled
		this.mockMvc.perform(get("/flight/1","json"))
		.andDo(print())
		.andExpect(status().is4xxClientError());		
	}
	
	//@Test
	public void testBuyTicket() throws Exception{
		
		System.out.println("testBuyTicket:Create Flight");
		MvcResult result = createFlight(flight);
		
		System.out.println("testBuyTicket:Create Flight:"+result.getResponse().getContentAsString());
		
		//Create User
		createUser(new User("Yuri","Scripnic","yuriscripnic@hotmail.com"));
		
		System.out.println("testBuyTicket:Buy Ticket 1");
		buyTicket(1,1,1);
		
		System.out.println("testBuyTicket:Buy Ticket 2");
		buyTicket(1,1,1);
		
		System.out.println("testBuyTicket: Verify if there are tickets");
		this.mockMvc.perform(get("/ticket","json"))
		.andDo(print())
		//.andExpect(jsonPath("$.ticket.").value(""));
		.andExpect(status().is2xxSuccessful());
	}
	
	//@Test
	public void testChecking() throws Exception{
		
		System.out.println("testChecking:Create Flight");
		createFlight(flight);
		
		System.out.println("testChecking:Create User");
		createUser(new User("Yuri","Scripnic","yuriscripnic@hotmail.com"));
		
		System.out.println("testChecking:Buy Ticket 1");
		buyTicket(1,1,1);
		
		System.out.println("testChecking: Checking Ticket 1");
		this.mockMvc.perform(get("/ticket/1/checking","json"))
		.andDo(print())
		//.andExpect(jsonPath("$.ticket.").value(""));
		.andExpect(status().is2xxSuccessful());
		
		System.out.println("testChecking: Verify Ticket 1");
		this.mockMvc.perform(get("/ticket/1"))
		.andDo(print())
		.andExpect(jsonPath("$.checkingDone").value(true))
		.andExpect(status().is2xxSuccessful());
	}
	
	@Test
	public void testPrintPass() throws Exception{
		
		createFlight(flight);
		
		createUser(new User("Yuri","Scripnic","yuriscripnic@hotmail.com"));
		
		System.out.println("testPrintPass:Buy Ticket 1");
		buyTicket(1,1,1);
		
		System.out.println("testPrintPass: Checking Ticket 1");
		this.mockMvc.perform(get("/ticket/1/print"))
		.andDo(print())
		//.andExpect(content().string("Reservation Ticket").)
		.andExpect(status().is2xxSuccessful());
	}	
	
}
