package airticket;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import airticket.model.Flight;

public class TestFlight extends RestTestTemplate{
	

	@Test
	public void testCrudFlight() throws Exception{
		byte[] msg = convertObjectToJson(new Flight("Test1",10)).getBytes();
		byte[] newmsg = convertObjectToJson(new Flight("Test2",15)).getBytes();
		
		//Create Flight
		this.mockMvc.perform(post("/flight")
		  .contentType(MediaType.APPLICATION_JSON)
		  .content(msg))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
		
		// Read Flight Data
		this.mockMvc.perform(get("/flight/1","json").accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(jsonPath("$.name").value("Test1"))
				.andExpect(jsonPath("$.numberOfSeats").value(10))
				.andExpect(status().isOk());
		
		// Update Flight Data
		this.mockMvc.perform(put("/flight/1","json")
				  .contentType(MediaType.APPLICATION_JSON)
				  .content(newmsg)
    			  .accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(jsonPath("$.name").value("Test2"))
				.andExpect(jsonPath("$.numberOfSeats").value(15))
				.andExpect(status().is2xxSuccessful());
		
		// Delete Flight Data
		this.mockMvc.perform(delete("/flight/1","json"))
				.andDo(print())
				.andExpect(status().is2xxSuccessful());
		
		// Verify if Flight is deleted
		this.mockMvc.perform(get("/flight/1","json"))
		.andDo(print())
		.andExpect(status().is4xxClientError());		
	}
	
}
