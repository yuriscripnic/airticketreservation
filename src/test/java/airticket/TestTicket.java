package airticket;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import airticket.model.Flight;
import airticket.model.Ticket;
import airticket.model.User;


public class TestTicket extends RestTestTemplate{
	
	@Test
	public void testCrudTicket() throws Exception{
		User user = new User ("Yuri","Scripnic","yuriscripnic@hotmail.com"); 
		Flight flight = new Flight("Flight 1",10);
		byte[] msg = convertObjectToJson(new Ticket(user,flight,1)).getBytes();
		byte[] newmsg = convertObjectToJson(new Ticket(user,flight,2)).getBytes();
		
		System.out.println(convertObjectToJson(new Ticket(user,flight,1)));
		//System.out.println(newmsg.toString());
		
		//Create Ticket
		this.mockMvc.perform(post("/ticket")
		  .contentType(MediaType.APPLICATION_JSON)
		  .content(msg))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
		
		// Read Ticket Data
		this.mockMvc.perform(get("/ticket/1","json").accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(jsonPath("$.seat").value(1))
				//.andExpect(jsonPath("$.user[1].firstName").value("Yuri"))
				.andExpect(status().isOk());
		
		// Update Ticket Data
		this.mockMvc.perform(put("/ticket/1","json")
				  .contentType(MediaType.APPLICATION_JSON)
				  .content(newmsg)
    			  .accept(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(jsonPath("$.seat").value(2))
				//.andExpect(jsonPath("$.user[1].firstName").value("Yuri"))
				.andExpect(status().is2xxSuccessful());
		
		// Delete Ticket Data
		this.mockMvc.perform(delete("/ticket/1","json"))
				.andDo(print())
				.andExpect(status().is2xxSuccessful());
		
		// Verify if Ticket is deleted
		this.mockMvc.perform(get("/ticket/1","json"))
		.andDo(print())
		.andExpect(status().is4xxClientError());		
	}
	
}
