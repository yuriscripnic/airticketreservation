package airticket;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.google.gson.Gson;

import java.io.IOException;


@RunWith(Suite.class)
@SuiteClasses({ TestUser.class,
				TestFlight.class,
				TestTicket.class	             
	          })

public class AirTicketTestSuit {
}
